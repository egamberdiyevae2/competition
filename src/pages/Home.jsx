import React, { Component } from "react";
// import Background from "../assets/background.svg";
import Play from "../assets/hover.svg";
import Img from "../assets/home-img.png";
import dot from "../assets/ellipse.svg";
import mainPage from "../assets/main-1.png";
import Check from "../assets/check.svg";
import { Link } from "react-router-dom";
import Person1 from "../assets/person1.png";
import Person2 from "../assets/person2.png";
import Person3 from "../assets/person3.png";
import Person4 from "../assets/person4.png";
import Person5 from "../assets/person5.png";
import Person6 from "../assets/image.png";

export default class Home extends Component {
  render() {
    return (
      <>
        <div className="home-section">
          <div className="top-items">
            <div className="top-left">
              <div className="player">
                <img src={Play} alt="img" />
                <p>Play showreel</p>
              </div>
              <h3>
                Enjoy studying <br /> with Createx <br /> Online Courses
              </h3>
              <div className="buttons">
                <Link to="/about">
                  <button>About us</button>
                </Link>
                <Link to="/course">
                  <button>Explore courses</button>
                </Link>
              </div>
            </div>
            <img src={Img} alt="" />
          </div>
          <div className="bottom-items">
            <div className="item">
              <h5>1200</h5>
              <p>Students graduated</p>
            </div>
            <img src={dot} alt="" />
            <div className="item">
              <h5>84</h5>
              <p>Completed courses</p>
            </div>
            <img src={dot} alt="" />
            <div className="item">
              <h5>16</h5>
              <p>Qualified tutors</p>
            </div>
            <img src={dot} alt="" />
            <div className="item">
              <h5>5</h5>
              <p>Years of experience</p>
            </div>
          </div>
        </div>
        <div className="main">
          <div className="cause">
            <img src={mainPage} alt="img" />
            <div className="right">
              <h6>Who we are</h6>
              <h4>Why Createx?</h4>
              <div className="itemss">
                <img src={Check} alt="" />
                <p>
                  A fermentum in morbi pretium aliquam adipiscing <br /> donec
                  tempus.
                </p>
              </div>
              <div className="itemss">
                <img src={Check} alt="" />
                <p>Vulputate placerat amet pulvinar lorem nisl.</p>
              </div>
              <div className="itemss">
                <img src={Check} alt="" />
                <p>
                  Consequat feugiat habitant gravida quisque elit <br />{" "}
                  bibendum id adipiscing sed.
                </p>
              </div>
              <div className="itemss">
                <img src={Check} alt="" />
                <p>Etiam duis lobortis in fames ultrices commodo nibh.</p>
              </div>
              <div className="itemss">
                <img src={Check} alt="" />
                <p>Tincidunt sagittis neque sem ac eget.</p>
              </div>
              <div className="itemss">
                <img src={Check} alt="" />
                <p>
                  Ultricies amet justo et eget quisque purus <br /> vulputate
                  dapibus tortor.
                </p>
              </div>
              <Link to="/about">
                <button>More about us</button>
              </Link>
            </div>
          </div>
          <div className="featured-course">
            <h5>Ready to learn?</h5>
            <div className="top-need">
              <h4>Featured Courses</h4>
              <Link to="/course">
                <button>View all courses</button>
              </Link>
            </div>
            <div className="feature-item">
              <div className="item">
                <img src={Person1} alt="" />
                <div className="item-des">
                  <h5>Marketing</h5>
                  <h4>
                    The Ultimate Google Ads Training <br /> Course
                  </h4>
                  <p>
                    <span>$100 </span>| by Jerome Bell
                  </p>
                </div>
              </div>
              <div className="item">
                <img src={Person4} alt="" />
                <div className="item-des">
                  <h5 style={{ background: "#5A87FC" }}>Management</h5>
                  <h4>
                    {" "}
                    The Ultimate Google Ads Training <br /> Course
                  </h4>
                  <p>
                    <span>$480 </span>| by Marvin McKinney
                  </p>
                </div>
              </div>
            </div>
            <div className="feature-item">
              <div className="item">
                <img src={Person2} alt="" />
                <div className="item-des">
                  <h5 style={{ background: "#F89828" }}>HR & Recruting</h5>
                  <h4>
                    {" "}
                    The Ultimate Google Ads Training <br /> Course
                  </h4>
                  <p>
                    <span>$200 </span>| by Leslie Alexander Li
                  </p>
                </div>
              </div>
              <div className="item">
                <img src={Person5} alt="" />
                <div className="item-des">
                  <h5>Marketing</h5>
                  <h4>
                    The Ultimate Google Ads Training <br /> Course
                  </h4>
                  <p>
                    <span>$530 </span>| by Kristin Watson
                  </p>
                </div>
              </div>
            </div>
            <div className="feature-item">
              <div className="item">
                <img src={Person3} alt="" />
                <div className="item-des">
                  <h5 style={{ background: "#5A87FC" }}>Management</h5>
                  <h4>
                    The Ultimate Google Ads Training <br /> Course
                  </h4>
                  <p>
                    <span>$400 </span>| by Dianne Russell
                  </p>
                </div>
              </div>
              <div className="item">
                <img src={Person6} alt="" />
                <div className="item-des">
                  <h5 style={{ background: "#F52F6E" }}>Design</h5>
                  <h4>
                    The Ultimate Google Ads Training <br /> Course
                  </h4>
                  <p>
                    <span>$100 </span>| by Guy Hawkins
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="benefits"></div>
        </div>
      </>
    );
  }
}
