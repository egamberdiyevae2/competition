import React, { Component } from "react";
import { Link } from "react-router-dom";
import Logo from "../assets/logo.svg";
import Profile from "../assets/Profile.svg";

export default class Nav extends Component {
  render() {
    return (
      <div className="nav-items">
        <img src={Logo} alt="logo" />
        <ul className="nav-links">
          <li>
            <Link className="link" to="/">
              Home
            </Link>
          </li>
          <li>
            <Link className="link" to="/about">
              About Us
            </Link>
          </li>
          <li>
            <Link className="link" to="/course">
              Courses
            </Link>
          </li>
          <li>
            <Link className="link" to="/event">
              Events
            </Link>
          </li>
          <li>
            <Link className="link" to="/blog">
              Blog
            </Link>
          </li>
          <li>
            <Link className="link" to="/contact">
              Contacts
            </Link>
          </li>
          <li>
            <Link to="/sign" className="btn-1">
              <button>Get consultation</button>
            </Link>
          </li>
          <li>
            <Link to="/sign">
              <button
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  gap: "5px",
                  marginLeft: "-10px",
                }}
              >
                <img src={Profile} alt="img" />
                Log in / Register
              </button>
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}
