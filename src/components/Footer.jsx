import React, { Component } from "react";
import Logo from "../assets/logo2.svg";
import Facebook from "../assets/Facebook.svg";
import Twitter from "../assets/Twitter.svg";
import Youtube from "../assets/YouTube.svg";
import Telegram from "../assets/telegram.svg";
import Instagram from "../assets/Instagram.svg";
import Linkedin from "../assets/Linked-In.svg";

export default class Footer extends Component {
  render() {
    return (
      <>
        <div className="footer-items">
          <div className="first-line">
            <img src={Logo} alt="" />
            <p>
              Createx Online School is a leader in online studying. <br />{" "}
              We have lots of courses and programs from the main <br /> market
              experts. We provide relevant approaches <br /> to online learning,
              internships and employment <br /> in the largest companies in the
              country.{" "}
            </p>
            <div className="icons">
              <img src={Facebook} alt="icon" />
              <img src={Twitter} alt="icon" />
              <img src={Youtube} alt="icon" />
              <img src={Telegram} alt="icon" />
              <img src={Instagram} alt="icon" />
              <img src={Linkedin} alt="icon" />
            </div>
          </div>
          <div className="other-line">
            <h5>SITE MAP</h5>
            <p>About Us</p>
            <p>Courses</p>
            <p>Events</p>
            <p>Blog</p>
            <p>Contacts</p>
          </div>
          <div className="other-line">
            <h5>COURSES</h5>
            <p>Marketing</p>
            <p>Management</p>
            <p>HR & Recruting</p>
            <p>Design</p>
            <p>Development</p>
          </div>
          <div className="other-line">
            <h5>CONTACT US</h5>
            <p>(405) 555-0128</p>
            <p>hello@createx.com</p>
          </div>
          <div className="other-line">
            <h5>SIGN UP TO OUR NEWSLETTER</h5>
            <input type="text" placeholder="Email address" />
            <p style={{ opacity: "1" }}>
              *Subscribe to our newsletter to receive communications and <br />{" "}
              early updates from Createx SEO Agency.
            </p>
          </div>
        </div>
        <div className="footer2">
          <p>© All rights reserved.Made with ❤️ by Createx Studio </p>
          <h5>GO TO TOP</h5>
        </div>
      </>
    );
  }
}
