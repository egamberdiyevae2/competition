import React, { Component } from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import Course from "./pages/Course";
import Event from "./pages/Event";
import Blog from "./pages/Blog";
import Contact from "./pages/Contact";

import Not from "./pages/Not";
import Sign from "./pages/Sign";
import Nav from "./components/Nav";
import Footer from "./components/Footer";

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Nav />
        <main>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/course" element={<Course />} />

            <Route path="/event" element={<Event />} />

            <Route path="/blog" element={<Blog />} />

            <Route path="/contact" element={<Contact />} />
            <Route path="/sign" element={<Sign />} />
            <Route path="/*" element={<Not />} />
          </Routes>
        </main>
        <Footer />
      </div>
    );
  }
}
